//au chargement on cache les 4 boutons.
$(document).ready(function () {
  cacherTousBoutons();
  cacherChampsReclamation();
});

//cacher champs authentification
function cacherChampsAuthentification() {
  $('#lblNom').hide();
  $('#txtNom').hide();
  $('#lblMotDePasse').hide();
  $('#txtMotDePasse').hide();
}

//le menu du login apres authentification.
function afficherMenuAuthentification() {
  $('#txtMessage').hide();
  $('#txtMessage').fadeIn(500);
  $('#boutonConnexion').hide();
  $('#boutonNouvelleDemande').fadeIn(500);
  $('#boutonPoursuivreDemande').fadeIn(500);
  $('#boutonDeconnexion').fadeIn(500); 
  $('#boutonInfoClient').fadeIn(500);  
  $('#txtMessage').html('Vous &#234;tes authentifi&#233;, choisisez une option.')
  
}

//Cacher boutons demande de reclamation.
function cacherTousBoutons() {
  $('#boutonEstime').hide();
  $('#boutonSoumettre').hide();
  $('#boutonNouvelleDemande').hide();
  $('#boutonPoursuivreDemande').hide();
  $('#boutonRetour').hide();
  $('#boutonDeconnexion').hide(); 
  $('#boutonSupprimerDemande').hide();  
  $('#boutonInfoClient').hide();    
  $('#lblNomPrenom').hide();     
  $('#txtNomPrenom').hide();     
};

//Afficher boutons demande de reclamation.
function afficherBoutonsReclamation() {
  $('#boutonEstime').fadeIn(1500); 
  $('#boutonSoumettre').fadeIn(1500);
  $('#boutonRetour').fadeIn(1500);
  $('#boutonSupprimerDemande').fadeIn(1500); 
};

//Cacher champs réclamations.
function cacherChampsReclamation() {
  $('#txtAddresse').hide();
  $('#txtNombresVoitures').hide();
  $('#txtNombreReclamations').hide();
  $('#txtNombrePersonnes').hide();
  $('#lblAddresse').hide();
  $('#lblNombreVoitures').hide();
  $('#lblNombreReclamations').hide();
  $('#lblNombrePersonnes').hide();
  $('#boutonRetour').hide(); 

};

//Afficher champs réclamations.
function afficherChampsClient() {
  $('#txtAddresse').hide();
  $('#lblAddresse').hide(); 
  $('#lblNomPrenom').hide();     
  $('#txtNomPrenom').hide();   

  $('#txtAddresse').fadeIn(1500); 
  $('#lblAddresse').fadeIn(1500); 
  $('#lblNomPrenom').fadeIn(1500);    
  $('#txtNomPrenom').fadeIn(1500); 
};

//Afficher champs réclamations.
function afficherChampsReclamation() { 
  $('#txtNombresVoitures').fadeIn(1500);
  $('#txtNombreReclamations').fadeIn(1500);
  $('#txtNombrePersonnes').fadeIn(1500); 
  $('#lblNombreVoitures').fadeIn(1500);
  $('#lblNombreReclamations').fadeIn(1500);
  $('#lblNombrePersonnes').fadeIn(1500);
};

//Clique sur authentification
$(function () {
  $("#boutonConnexion").click(function () {
    var nom = $("#txtNom").val();
    var motDePasse = $("#txtMotDePasse").val();

    if (validerAuthentification(nom, motDePasse) == true) {  
      cacherChampsAuthentification(); 
      afficherMenuAuthentification();
    } else {
      $('#txtMessage').text('Veuillez entrer le bon nom de compte et le bon mot de passe.')
    }
  });
});

function viderChampsReclamation(){ 
  $("#txtNombresVoitures").val('');  
  $("#txtNombreReclamations").val('');
  $("#txtNombrePersonnes").val(''); 
}

//Clique sur Nouvelle demande
$(function () {
  $("#boutonNouvelleDemande").click(function () { 
    $('#boutonDeconnexion').hide();   
    cacherTousBoutons();
    afficherChampsReclamation();
    afficherBoutonsReclamation();
    viderChampsReclamation();
     $('#txtMessage').hide();
    $('#txtMessage').fadeIn(1500);
    $('#txtMessage').text('Veuillez remplir le formulaire.')
    $('#boutonDeconnexion').hide();  
  });
});

//Clique sur continuer demande
$(function () {
  $("#boutonPoursuivreDemande").click(function () {
    $('#boutonDeconnexion').hide();  
    cacherTousBoutons();
    afficherChampsReclamation();
    afficherBoutonsReclamation();
    $('#txtMessage').hide();
    $('#txtMessage').fadeIn(1500);
    $('#txtMessage').text('Veuillez remplir le formulaire.')
    $('#boutonDeconnexion').hide();   
  });
});

//Verifier nom et mot de passe
function validerAuthentification(nom, motDePasse) {
  return (nom.toUpperCase() == "JEAN" && motDePasse.toUpperCase() == "TREMBLAY") ? true : false;
}

//deconnexion
$(function () {
  $("#boutonDeconnexion").click(function () {
    $('#txtMessage').html('Au revoir.') 
    cacherTousBoutons(); 
    $('#boutonConnexion').show();  
    $('#lblNom').show(); 
    $('#txtNom').show(); 
    $('#lblMotDePasse').show(); 
    $('#txtMotDePasse').show();  
  });
});

function afficherMessage(message) {
  $('#txtMessage').hide();
  $('#txtMessage').fadeIn(1500);
  $('#txtMessage').html(message)
}

function calculerAlgoPrix(nbVoitures, nbReclamations, nbPersonnes) {
  return nbVoitures * 25 + 30 * nbReclamations + nbPersonnes * 15 + 15;
}

function verificationParametres(nbVoitures, nbReclamations, nbPersonnes) {
  if (nbVoitures == "" || nbReclamations == "" || nbPersonnes == "") {
    afficherMessage("Veuillez remplir tous les champs.");
  } else if (nbPersonnes <= 0) {
    afficherMessage("Il doit avoir au moins une personne a assurer.");
  } else if (isNaN(nbVoitures) || isNaN(nbReclamations) || isNaN(nbPersonnes)) {
    afficherMessage("Les champs doivent &#234;tre num&#233;riques.");
  } else {
    return true;
  }
  return false;
}

//estime du cout 
$(function () {
  $("#boutonEstime").click(function () { 
    var nbVoitures = $('#txtNombresVoitures').val();
    var nbReclamations = $('#txtNombreReclamations').val();
    var nbPersonnes = $('#txtNombrePersonnes').val();

    if (verificationParametres(nbVoitures, nbReclamations, nbPersonnes)) {
      var prix = calculerAlgoPrix(nbVoitures, nbReclamations, nbPersonnes);
      afficherMessage('Estim&#233; = ' + prix + '$ par mois. <br/> Estim&#233; =  ' + (prix * 12) + ' $ par ann&#233;e.')
    }

  });
});

//Retour menu
$(function () {
  $("#boutonRetour").click(function () {
    cacherChampsReclamation();
    cacherTousBoutons();
    afficherMenuAuthentification();
  });
});

//Supprimer demande
$(function () {
  $("#boutonSupprimerDemande").click(function () {
    cacherChampsReclamation();
    cacherTousBoutons();
    afficherMenuAuthentification();
    viderChampsReclamation(); 
  });
});

//Afficher info client
$(function () {
  $("#boutonInfoClient").click(function () {
    cacherChampsReclamation();
    cacherTousBoutons(); 
    afficherChampsClient();
    $("#boutonRetour").fadeIn(1500);
    afficherMessage('Veuillez entrer vos informations personnelles.');
  });
});

//appel du serveur
$(function () {
  $("#boutonSoumettre").click(function () {
 
    var nbVoitures = $('#txtNombresVoitures').val()
    var nbReclamations = $('#txtNombreReclamations').val()
    var nbPersonnes = $('#txtNombrePersonnes').val()
    var valeurMessageClient = nbVoitures + ';' + nbReclamations + ';' + nbPersonnes;

    if (verificationParametres(nbVoitures, nbReclamations, nbPersonnes)) { 
      var temp = {
        messageClient: valeurMessageClient
      };

      $.ajax({
        url: "backend.php",
        type: "POST",
        data: temp,
        dataType: "text",
        success: function (response) {
          var reponseServeur = response;
          $("#txtMessage").html(reponseServeur);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          alert('Une erreur est arrivee\ntextStatus = ' + textStatus + '\nerreur = ' + errorThrown + '\nstatus = ' + XMLHttpRequest.status);
        }
      }); 
    } 
  });
});