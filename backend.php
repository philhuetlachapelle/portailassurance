<?php 
  
  if (isset($_POST['messageClient'])){  
     $parametres = xssFiltre($_POST['messageClient']);
     if($parametres != ''){
           $valeurs = explode(';', $parametres);
           $nbVoitures = $valeurs[0];
           $nbReclamations = $valeurs[1]; 
           $nbPersonnes = $valeurs[2];
           $total = calculAlgo($nbVoitures,$nbReclamations,$nbPersonnes);
           echo 'La soumission de ' . strval($nbVoitures) . ' voiture(s) et de ' . strval($nbPersonnes) . ' personne(s) pour un total de ' . strval($total) . 
          '$ par mois et ' . strval($total * 12) . '$  par année a été envoyée. Voici votre code de confirmation ' . rand ( 0 , 99999 ) . '.';
     }
   
  };
   
  function xssFiltre($donnes,$encoding='UTF-8')
  {
     return htmlspecialchars($donnes,ENT_QUOTES | ENT_HTML401,$encoding);
  }

  function calculAlgo($nbVoitures,$nbReclamations,$nbPersonnes){
      return $nbVoitures * 25 + 30 * $nbReclamations + $nbPersonnes * 15 + 15;
  }

?>